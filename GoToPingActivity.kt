package com.example.testono

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET

data class ResultBody(
        val status: String
)

interface OnoService {
    @GET("ping")
    fun doPing() : Call<ResultBody>
}

class GoToPingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_go_to_ping)
    }

    fun pingOno(view: View) {
        view.isEnabled = false;






        val mySpinner = findViewById<ProgressBar>(R.id.indeterminateBar).apply {
            visibility =  View.VISIBLE
        }

        val request = ServiceBuilder.buildService(OnoService::class.java)
        val call = request.doPing()

        Log.v("TestingLog","Button Clicked")

        call.enqueue(object : Callback<ResultBody> {
            override fun onResponse(call: Call<ResultBody>, response: Response<ResultBody>) {
                if (response.isSuccessful){
                    Log.v("TestingLog","success")
                    Log.v("TestingLog",response.body().toString())
                    val textView = findViewById<TextView>(R.id.textView).apply {
                        text =  response.body()!!.status
                    }
                }

                view.isEnabled = true;
                mySpinner.apply { visibility =  View.INVISIBLE}
            }
            override fun onFailure(call: Call<ResultBody>, t: Throwable) {
                Log.v("TestingLog","failure")
                Log.v("TestingLog",t.toString())

                val textView = findViewById<TextView>(R.id.textView).apply {
                    text = "Failed"
                }

                view.isEnabled = true;
                mySpinner.apply { visibility =  View.INVISIBLE}
            }
        })
    }
}