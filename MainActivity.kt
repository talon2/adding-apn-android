package com.example.testono

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.data.ApnSetting
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity


const val EXTRA_MESSAGE = "com.example.testono.MESSAGE"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }




    fun goToTest(view: View) {

        // Create an MMS proxy address with a hostname. A network might not be
        // available, so supply a placeholder (0.0.0.0) IPv4 address to avoid DNS lookup.
        // Create an MMS proxy address with a hostname. A network might not be
        // available, so supply a placeholder (0.0.0.0) IPv4 address to avoid DNS lookup.
        val host = "mms.example.com"
        val ipAddress = ByteArray(4)

        //    "{\"mcc\":\"23450\",\"name\":\"hologram\",\"username\":\"\",\"password\":\"\",\"spn\":\"Hologram\"},"
        //      APN: hologram
        //      APN username: (none)
        //      APN password: (none)
        //      IP Address: Dynamic (using DHCP)
        //      Data Roaming: Enabled

        val apn: ApnSetting = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                ApnSetting.Builder()
                        .setApnTypeBitmask(ApnSetting.TYPE_DEFAULT)
                        .setApnName("hologram")
                        .setEntryName("hologram")
                        .setRoamingProtocol(ApnSetting.PROTOCOL_IP)
                        .setOperatorNumeric("23450")
                        .build()
            } else {
                TODO("VERSION.SDK_INT < Q")
            }
        } else {
            TODO("VERSION.SDK_INT < P")
        }

//
//        val componentName = ComponentName(this, AdminReceiver::class.java)
//        val devicePolicyManager = applicationContext.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
//        devicePolicyManager.setOverrideApnsEnabled(componentName, true);
//        devicePolicyManager.addOverrideApn(componentName, apn)
//        devicePolicyManager.setOverrideApnsEnabled(componentName, true);

        Log.d("SET_APN", "version " + android.os.Build.VERSION.SDK_INT + apn.getApnName() + " is enabled: " + apn.isEnabled())
        Log.d("SET_APN", "set apn param")
        Log.d("SET_APN", "APN SETTING " + apn.toString())



        val message = "Ping Ono"
        val intent = Intent(this, GoToPingActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, message)
        }
        startActivity(intent)
    }
}