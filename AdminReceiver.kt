package com.example.testono

import android.R
import android.app.admin.DeviceAdminReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast


class AdminReceiver : DeviceAdminReceiver() {
    override fun onEnabled(context: Context, intent: Intent?) {
        Toast.makeText(
            context,
            "admin enabled",
            Toast.LENGTH_SHORT
        ).show()

        Log.d("ADMINRECEIVER", "Admin enabled")
    }

    override fun onDisableRequested(context: Context, intent: Intent?): CharSequence {
        Log.d("ADMINRECEIVER", "Admin disable requested")
        return "Disable requested"
    }

    override fun onDisabled(context: Context, intent: Intent?) {
        Toast.makeText(
            context,
            "admin disabled",
            Toast.LENGTH_SHORT
        ).show()
        Log.d("ADMINRECEIVER", "Admin disabled")
    }
}